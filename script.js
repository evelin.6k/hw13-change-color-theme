let changeThemeBtn = document.getElementById("theme-button");
let themeLink = document.getElementById("theme-link");

if(localStorage.getItem('data-theme') === 'dark'){
    themeLink.setAttribute('href', "./css/dark.css")
}
else if(localStorage.getItem('data-theme') === 'light'){
    themeLink.setAttribute('href', "./css/light.css")
}

changeThemeBtn.addEventListener("click", ChangeTheme);

function ChangeTheme()
{
    let lightTheme = "./css/light.css";
    let darkTheme = "./css/dark.css";
    let currTheme = themeLink.getAttribute("href");

    if(currTheme == lightTheme){
   	    currTheme = darkTheme;
        window.localStorage.setItem('data-theme', 'dark');
    }
    else{    
   	    currTheme = lightTheme;
        window.localStorage.setItem('data-theme', 'light');
    }

    themeLink.setAttribute("href", currTheme);
}
